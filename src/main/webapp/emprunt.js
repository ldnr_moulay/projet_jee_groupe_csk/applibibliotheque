

//Créer les emprunts

$(function () {
    $("#nouveau").on('click', function () {
        let livreEnregistre = $("#livreEnregistre").val()
        let nomEmprunteur = $("#nomEmprunteur").val()
        let prenomEmprunteur = $("#prenomEmprunteur").val()
        let dateEmprunt = $("#dateEmprunt").val()
        $("#message_erreur").html("")
        $("#message").html("création nouvel emprunt...")
        $("#message_erreur").html("")
        $("#message").html("Création d'un emprunt")
        $.ajax({
            url: "/emprunt/nouvelEmprunt",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                "livreEnregistre": livreEnregistre,
                "nomEmprunteur": nomEmprunteur,
                "prenomEmprunteur": prenomEmprunteur,
                "dateEmprunt": dateEmprunt
            })

        }).done(function (message) { // fichier js recupere ce que renvoie le controlleur à l'url : /emprunt/nouveau

            let cond = ""
            //console.log(message)
            //console.log(message.substring(0, 10))
            if (message.substring(0, 10) !== ("Un nouvel ")) {
                $("#message_erreur").html(message)
                $("#message").html("")
            } else {
                $("#message").html(message)
            }

        }).fail(function (message) {
            $("#message").html("")
            $("#message_erreur").html("Echec veuillez vérifier les informations entrées")
        })
    })

    //Get titre des livres pour option "titre du livre"
    $.ajax({
        url: "/livre/enregistrement/livres",
        type: "GET",
        dataType: "json"
    })
            .done(function (message) { // fichier js recupere ce que renvoie le controlleur à l'url
                $("#messageLivres").html(message)
                let lignes = "<option value=0>aucun titre</option> "
                for (const ligne of message) {
                    lignes += "<option value=" + ligne.idLivre + ">" + ligne.idLivre + " " + ligne.titre + "</option>"
                }
                $("#livreEnregistre").html(lignes)

            }).fail(function (message) {
        $("#messageLivres").html("Echec " + JSON.stringify(message))
    })



    //Afficher tous les emprunts en cours
    $("#affichage").on('click', function () {
        $("#message").html("affichage des emprunts en cours")
        $("#message_erreur").html("")
        $.ajax({
            url: "/emprunt/all",
            type: "GET",
            dataType: "json"
        })
                .done(function (message) { // fichier js recupere ce que renvoie le controlleur à l'url (en json)
                    $("#affichageEmprunts tbody").html(message)
                    let lignes = ""
                    for (const ligne of message) {
                        let date = ligne.dateEmprunt.substring(0, 10)
                        lignes += "<tr>" +
                                "<td>" + ligne.idEmprunt + "</td>" +
                                "<td>" + ligne.livre.idLivre + "</td>" +
                                "<td>" + ligne.livre.titre + "</td>" +
                                "<td>" + ligne.nomEmprunteur + "</td>" +
                                "<td>" + ligne.prenomEmprunteur + "</td>" +
                                "<td>" + date + "</td>" +
                                "</tr>"
                    }
                    $("#affichageEmprunts tbody").html(lignes)

                }).fail(function (message) {
            $("#message_erreur").html("Echec veuillez redémarrer l'application ou contacter la maintenance")
            $("#message").html("")
        })
    })

    $("#modifier").on('click', function () {
        let idEmprunt = $("#idEmprunt").val()
        let dateRetour = $("#dateRetour").val()
        $("#message").html("modification en cours")
        $("#message_erreur").html("")
        $.ajax({
            url: "/emprunt/modifier",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                "idEmprunt": idEmprunt,
                "dateRetour": dateRetour,
            })
        }).done(function (message) {

            if (message.substring(0, 9) === "L'emprunt") {
                $("#message").html(message)
            } else {
                $("#message_erreur").html(message)
                $("#message").html("")
            }
        }).fail(function () {
            $("#message").html("Echec veuillez vérifier les informations entrées")
        })
    })
    /* *** Restes d'une ancienne version du code ***
     * 
     * 
     * $("#rechercher").on('click', function() {
     let nomEmprunteur = $("#nomEmprunteur").val()
     let prenomEmprunteur = $("#prenomEmprunteur").val()
     $.ajax({
     url: "/emprunt/searchbyname",
     type: "POST",
     dataType: "json",
     contentType: "application/json",
     data: JSON.stringify({
     "nomEmprunteur": nomEmprunteur,
     "prenomEmprunteur": prenomEmprunteur
     })
     }).done(function(list) { // fichier js recupere ce que renvoie le controlleur à l'url (en json)
     $("#affichageEmprunts tbody").html(list)
     let lignes = ""
     for (const ligne of list) {
     lignes += "<tr>" +
     "<td>" + ligne.idEmprunt + "</td>" +
     "<td>" + ligne.livre.idLivre + "</td>" +
     "<td>" + ligne.livre.titre + "</td>" +
     "<td>" + ligne.nomEmprunteur + "</td>" +
     "<td>" + ligne.prenomEmprunteur + "</td>" +
     "<td>" + ligne.dateEmprunt + "</td>" +
     "</tr>"
     }
     $("#affichageEmprunts tbody").html(lignes)
     }).fail(function(message) {
     $("#affichageEmprunts tbody").html("Echec " + message)
     })
     })
     $("#rechercher").on('click', function() {
     let livreEnregistre = $("#livreEnregistre").val()
     $.ajax({
     url: "/emprunt/searchbytitle",
     type: "POST",
     dataType: "json",
     contentType: "application/json",
     data: JSON.stringify({
     "livreEnregistre": livreEnregistre
     })
     }).done(function(list) { // fichier js recupere ce que renvoie le controlleur à l'url (en json)
     $("#affichageEmprunts tbody").html(list)
     let lignes = ""
     for (const ligne of list) {
     lignes += "<tr>" +
     "<td>" + ligne.idEmprunt + "</td>" +
     "<td>" + ligne.livre.idLivre + "</td>" +
     "<td>" + ligne.livre.titre + "</td>" +
     "<td>" + ligne.nomEmprunteur + "</td>" +
     "<td>" + ligne.prenomEmprunteur + "</td>" +
     "<td>" + ligne.dateEmprunt + "</td>" +
     "</tr>"
     }
     $("#affichageEmprunts tbody").html(lignes)
     }).fail(function(message) {
     $("#affichageEmprunts tbody").html("Echec " + message)
     })
     })*/
    //Afficher l'emprunt en cours correspondant à un idEmprunt
    $("#rechercher").on('click', function () {
        $("#message").html("Recherche en cours")
        $("#message_erreur").html("")
        let idEmprunt = $("#idEmprunt").val()
        let livreEnregistre = $("#livreEnregistre").val()
        let nomEmprunteur = $("#nomEmprunteur").val();
        let prenomEmprunteur = $("#prenomEmprunteur").val()
        //console.log("je cherche")
        $.ajax({
            url: "/emprunt/searchbyid",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({
                "idEmprunt": idEmprunt,
                "livreEnregistre": livreEnregistre,
                "nomEmprunteur": nomEmprunteur,
                "prenomEmprunteur": prenomEmprunteur
            })
        }).done(function (list) { // fichier js recupere ce que renvoie le controlleur à l'url (en json)
            $("#affichageEmprunts tbody").html(list)
            //console.log("je trouve")
            let lignes = ""
            for (resultat of list) {
                let date = resultat.dateEmprunt.substring(0, 10)
                lignes += "<tr>" +
                        "<td>" + resultat.idEmprunt + "</td>" +
                        "<td>" + resultat.livre.idLivre + "</td>" +
                        "<td>" + resultat.livre.titre + "</td>" +
                        "<td>" + resultat.nomEmprunteur + "</td>" +
                        "<td>" + resultat.prenomEmprunteur + "</td>" +
                        "<td>" + date + "</td>" +
                        "</tr>"
            }
            if (lignes === "") {
                $("#message").html("Aucun emprunt ne correspond à votre recherche")
            }
            $("#affichageEmprunts tbody").html(lignes)

        }).fail(function () {
            //console.log("je trouve pas")
            $("#message").html("Aucun emprunt ne correspond à votre recherche")
            $("#affichageEmprunts tbody").html("")
        })
    })
})

