
$(function () {
    $("#enregistrer").on('click', function() {
        $("#message").html("Envoi !")
        $("#message_erreur").html("")
        let titre = $("#titre").val()
        let annee = $("#annee").val()
        let editeur = $("#editeur").val()
        let nomAuteur = $("#nomAuteur").val()
        let prenomAuteur = $("#prenomAuteur").val()
        $.ajax({
            url: "/livre/enregistrement",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({
                "titre": titre,
                "annee": annee,
                "editeur": editeur,
                "nomAuteur": nomAuteur,
                "prenomAuteur": prenomAuteur
            })
        }).done(function (message) {
            if(message.substring(0,2)==="Un"){
            $("#message").html(message)}else{
            $("#message_erreur").html(message)
            $("#message").html("")
            }
        }).fail(function (message) {
            $("#message").html("Echec " + message)
        })
    })
  })
