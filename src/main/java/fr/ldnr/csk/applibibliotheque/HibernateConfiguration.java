/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.csk.applibibliotheque;

import java.util.Properties;

import org.slf4j.Logger;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;





@Configuration //la classe sera lue automatiquement par Spring pour chercher l'expression des beans qui seront utilisés ailleurs
public class HibernateConfiguration {
	
	public static final Logger logger = LoggerFactory.getLogger(HibernateConfiguration.class);
	@Bean // annotation pour déclaration d'un bean à la place d'une balise <bean> dans le fichier .xml
	public SessionFactory sessionFactory() {// sessionFactory a été mis dans le cache de Spring => @Autowired le retrouvera
		//ATTENTION : bien mettre un @Autowired dans le controleur où sera créée la SessionFactory
		Properties options = new Properties();
		options.put("hibernate.dialect", "org.sqlite.hibernate.dialect.SQLiteDialect");
		options.put("hibernate.hbm2ddl.auto","create");
		options.put("hibernate.show_sql","true");
		options.put("hibernate.connection.driver.class", "org.sqlite.JDBC");
		options.put("hibernate.connection.url", "jdbc:sqlite:bibliotheque.sqlite");
		SessionFactory factory = new org.hibernate.cfg.Configuration().
			addProperties(options).
			addAnnotatedClass(Livre.class).
                        addAnnotatedClass(Emprunt.class).buildSessionFactory();
		logger.info("SessionFactory créée");
		return factory;	
	}
        

}

