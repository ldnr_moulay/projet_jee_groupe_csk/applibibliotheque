package fr.ldnr.csk.applibibliotheque;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author stag
 */
@RestController
@RequestMapping("/emprunt")
public class EmpruntController {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public static final Logger logger = LoggerFactory.getLogger(EmpruntController.class);

    @ResponseBody
    @RequestMapping(value = "/nouvelEmprunt", method = RequestMethod.POST)
    public String envoi(@RequestBody Formulaire formulaire) {
        try {
            Session session = sessionFactory.openSession();
            Date date = new Date();
            if (formulaire.getLivreEnregistre() != 0) {
                if (!getLivre(session, formulaire.getLivreEnregistre()).isIsEmprunte()) {
                    if (!(formulaire.getNomEmprunteur().equals("") || formulaire.getPrenomEmprunteur().equals("") || formulaire.getDateEmprunt() == null)) {
                        if ((formulaire.getDateEmprunt().compareTo(date)) <= 0) {
                            logger.info("Enregistrement d'un nouvel emprunt : " + formulaire.getNomEmprunteur());

                            logger.info("je récupère " + formulaire.getLivreEnregistre());
                            Livre livre = getLivre(session, formulaire.getLivreEnregistre());
                            logger.info("j'ai récupéré : " + livre.getIdLivre());
                            Transaction tx = session.beginTransaction();
                            Emprunt emprunt = new Emprunt(livre, formulaire.getNomEmprunteur(), formulaire.getPrenomEmprunteur(), formulaire.getDateEmprunt());
                            emprunt.getLivre().setIsEmprunte(true);
                            logger.info(emprunt.getNomEmprunteur());
                            session.save(emprunt);
                            tx.commit();
                            session.close();
                            return "Un nouvel emprunt a été enregistré avec succès";
                        } else {
                            throw new IllegalArgumentException("Erreur : la date renseignée est dans le futur");
                        }
                    } else {
                        throw new IllegalArgumentException("Erreur : un champs a été laissé vide");
                    }
                } else {
                    throw new IllegalArgumentException("Erreur : veuillez selectionner un livre qui n'est pas emprunté");
                }
            } else {
                throw new IllegalArgumentException("Erreur : veuillez selectionner un livre");
            }
        } catch (IllegalArgumentException e) {
            return e.getMessage();
        }
    }

    //afficher tous les emprunts
    @RequestMapping("/all")
    public List<Emprunt> lireAllEmprunts() {
        //logger.info("titre du livre de l'emprunt : ");
        Session session = sessionFactory.openSession();
        String requeteHQL = "from Emprunt where dateRetour is NULL order by dateEmprunt asc";
        //from Emprunt inner join Livre on Emprunt.livre_idLivre=Livre.idLivre where isEmprunte order by Emprunt.dateEmprunt

        List<Emprunt> list = session.createQuery(requeteHQL).list();
        session.close();
        return list;
    }

    //recherche en fonction de l'id de l'emprunt
    @ResponseBody
    @RequestMapping(value = "/searchbyid", method = RequestMethod.POST)
    public List<Emprunt> lireID(@RequestBody Formulaire formulaire) {
        boolean isNomRempli = false;
        //logger.debug("je recntre dans la recherche : "+formulaire.getIdEmprunt()+formulaire.getLivreEnregistre()+formulaire.getNomEmprunteur()+formulaire.getPrenomEmprunteur());
        if (formulaire.getIdEmprunt() != 0 || formulaire.getLivreEnregistre() != 0 || !(formulaire.getNomEmprunteur().equals("")) || !(formulaire.getPrenomEmprunteur().equals(""))) {
            //logger.info("id de l'emprunt : " + formulaire.getIdEmprunt());
            //logger.info("id du livre : " + formulaire.getLivreEnregistre());

            logger.info("nom emprunteur : " + formulaire.getNomEmprunteur() + " prenom emprunteur : " + formulaire.getPrenomEmprunteur());
            Session session = sessionFactory.openSession();
            String requeteHQL = "from Emprunt where";
            String lien = " ";
            if (formulaire.getIdEmprunt() != 0) {
                requeteHQL += lien + "idEmprunt=" + formulaire.getIdEmprunt();
                lien = " and ";

            }
            if (formulaire.getLivreEnregistre() != 0) {
                Livre livre = getLivre(session, formulaire.getLivreEnregistre());
                String requeteTitreHQL = "select idLivre from Livre where titre=:t";
                List<Integer> list = session.createQuery(requeteTitreHQL).setParameter("t", livre.getTitre()).list();
                requeteHQL += lien;
                requeteHQL += "(";
                lien = "";

                for (Integer i : list) {
                    requeteHQL += lien + "Livre_idLivre like " + i;
                    lien = " or ";
                }
                requeteHQL += ")";
                lien = " and ";
            }
            if (!(formulaire.getNomEmprunteur().equals("") || formulaire.getPrenomEmprunteur().equals(""))) {
                logger.info("true");
            } else {
                logger.info("false");
            }
            if (!(formulaire.getNomEmprunteur().equals("") || formulaire.getPrenomEmprunteur().equals(""))) {
                requeteHQL += lien + "nomEmprunteur like :ne and prenomEmprunteur like :pe";
                lien = " and ";
                isNomRempli = true;
            }

            Transaction tx = session.beginTransaction();
            requeteHQL += " order by dateEmprunt asc";
            logger.debug(requeteHQL);
            if (isNomRempli) {
                List<Emprunt> resultat = session.createQuery(requeteHQL).setParameter("ne", formulaire.getNomEmprunteur()).setParameter("pe", formulaire.getPrenomEmprunteur()).list();
                tx.commit();
            session.close();
            return resultat;
            } else {
                List<Emprunt> resultat = session.createQuery(requeteHQL).list();
                tx.commit();
            session.close();
            return resultat;
            }
        } else {
            return null;
        }
    }

    private Livre getLivre(Session session, int livreId) {
        Transaction tx = session.beginTransaction();
        String livreHQL = "from Livre where idLivre=:i";
        Livre livre = (Livre) session.createQuery(livreHQL).setParameter("i", livreId).uniqueResult();
        tx.commit();
        return livre;
    }

    // Modification des emprunts
    @RequestMapping(value = "/modifier", method = RequestMethod.POST)
    public String modifierEmprunt(@RequestBody Formulaire formulaire) {
        try {
            Session session = sessionFactory.openSession();
            Date date = new Date();
            String getEmpruntHQL = "from Emprunt where idEmprunt=:i and dateRetour is NULL";
            if ((formulaire.getIdEmprunt() != 0) || (formulaire.getDateRetour() != null)) {
                System.out.println("je passe le 1 if de modifier");
                Emprunt emprunt = (Emprunt) session.createQuery(getEmpruntHQL).setParameter("i", formulaire.getIdEmprunt()).uniqueResult();
                if ((formulaire.getDateRetour().compareTo(date)) <= 0) {
                    System.out.println("je passe le 2 if de modifier");
                    if ((formulaire.getDateRetour().compareTo(emprunt.getDateEmprunt())) >= 0) {
                        System.out.println("je passe le 3 if de modifier");
                        logger.debug(formulaire.getIdEmprunt() + " " + formulaire.getDateRetour());
                        Transaction tx = session.beginTransaction();
                        emprunt.setDateRetour(formulaire.getDateRetour());
                        emprunt.getLivre().setIsEmprunte(false);
                        session.save(emprunt);
                        tx.commit();
                        session.close();
                        return "L'emprunt n°" + emprunt.getIdEmprunt() + " du livre : " + emprunt.getLivre().getTitre() + " n°" + emprunt.getLivre().getIdLivre() + " emprunté le : " + emprunt.getDateEmprunt().toString().substring(0, 10) + " par " + emprunt.getPrenomEmprunteur() + " " + emprunt.getNomEmprunteur() + " a été rendu le " + emprunt.getDateRetour().toString().substring(0, 10);
                    } else {
                        throw new IllegalArgumentException("Erreur : la date de retour est antérieur à la date d'emprunt !");
                    }
                } else {
                    throw new IllegalArgumentException("Erreur : la date de retour est dans le futur !");
                }
            } else {
                throw new IllegalArgumentException("Erreur : Veuillez entrez correctement l'id et la date de retour !");
            }
        } catch (Exception e) {
            return "Erreur : veuillez entrer l'id et la date de retour";
        }
    }
}
