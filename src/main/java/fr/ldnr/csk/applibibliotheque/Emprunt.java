/*
 * 
 */
package fr.ldnr.csk.applibibliotheque;

/**
 * @author Celine
 */
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class Emprunt implements Serializable {

    public static final long serialVersionUID = 1L;
    private int idEmprunt;
    private Livre livre;
    private String nomEmprunteur;
    private String prenomEmprunteur;
    private Date dateEmprunt;
    private Date dateRetour;

    public Emprunt(Livre livre, String nomEmprunteur, String prenomEmprunteur, Date dateEmprunt) {
        this.livre = livre;
        this.nomEmprunteur = nomEmprunteur;
        this.prenomEmprunteur = prenomEmprunteur;
        this.dateEmprunt=dateEmprunt;
    }

    public Emprunt() {
    }

    @Override
    public String toString() {
        return dateEmprunt + " " + idEmprunt + " " + livre + " " + nomEmprunteur + " " + prenomEmprunteur;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdEmprunt() {
        return idEmprunt;
    }

    public void setIdEmprunt(int idEmprunt) {
        this.idEmprunt = idEmprunt;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }

    @Column(length = 50, nullable = false)
    public String getNomEmprunteur() {
        return nomEmprunteur;
    }

    public void setNomEmprunteur(String nomEmprunteur) {
        this.nomEmprunteur = nomEmprunteur;
    }

    @Column(length = 30, nullable = false)
    public String getPrenomEmprunteur() {
        return prenomEmprunteur;
    }

    public void setPrenomEmprunteur(String prenomEmprunteur) {
        this.prenomEmprunteur = prenomEmprunteur;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateEmprunt() {
        

        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
//        Logger logger = LoggerFactory.getLogger(Emprunt.class);
//        logger.info("je passe par le setDateEmprunt");
        //livre.setIsEmprunte(true);
        this.dateEmprunt = dateEmprunt;
    }

    @Temporal(TemporalType.TIMESTAMP)
    public Date getDateRetour() {

        return dateRetour;
    }

    public void setDateRetour(Date dateRetour) {
//        Logger logger = LoggerFactory.getLogger(Emprunt.class);
//        logger.info("je passe par le setDateRetour");
               // livre.setIsEmprunte(false);
        this.dateRetour = dateRetour;
    }

}
