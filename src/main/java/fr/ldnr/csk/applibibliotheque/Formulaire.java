/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.csk.applibibliotheque;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Utilisateur
 */
public class Formulaire {
    private int livreEnregistre;
    private int idEmprunt;
    private String nomEmprunteur;
    private String prenomEmprunteur;  
    private Livre livre;
    private Date dateEmprunt;
    private Date dateRetour;  

    public int getLivreEnregistre() {
        return livreEnregistre;
    }

    public int getIdEmprunt() {
        return idEmprunt;
    }

    public String getNomEmprunteur() {
        return nomEmprunteur;
    }

    public String getPrenomEmprunteur() {
        return prenomEmprunteur;
    }

    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public Date getDateRetour() {
        return dateRetour;
    }

    public void setLivreEnregistre(int livreId) {
        this.livreEnregistre = livreId;
    }

    public void setNomEmprunteur(String nomEmprunteur) {
        this.nomEmprunteur = nomEmprunteur;
    }

    public void setPrenomEmprunteur(String prenomEmprunteur) {
        this.prenomEmprunteur = prenomEmprunteur;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
        
    }

    public void setDateRetour(Date dateRetour) {
        this.dateRetour = dateRetour;
    }
    
}
