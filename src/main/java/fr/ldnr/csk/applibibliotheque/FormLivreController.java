package fr.ldnr.csk.applibibliotheque;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/livre")
public class FormLivreController {
    private SessionFactory sessionFactory;
    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public static final Logger logger = LoggerFactory.getLogger(FormLivreController.class);

    @ResponseBody
    @RequestMapping(value = "/enregistrement", method = RequestMethod.POST)
    public String envoi(@RequestBody Livre livre) {
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        try {
            if (!(livre.getAnnee() == 0 || livre.getAnnee() > curYear || livre.getTitre().equals(null) || livre.getEditeur().equals("")
                    || livre.getNomAuteur().equals("") || livre.getPrenomAuteur().equals(""))) {
                logger.info("enregistrement de : " + livre.getTitre());
                Session session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                session.save(livre);
                tx.commit();
                session.close();
                return "Un nouveau livre a été enregistré avec succès";
            } else {
                throw new IllegalArgumentException("Erreur : problème dans un champ, veuillez vérifier s'il vous plait");
            }
        } catch (IllegalArgumentException e) {
            return e.getMessage();
        }
    }

    @RequestMapping("/enregistrement/livres")
    public List<Livre> lire() {
        logger.info("Get livres");
        Session session = sessionFactory.openSession();
        logger.debug("tout va bien");
        String requeteHQL = "from Livre";
        try{
        List<Livre> list = session.createQuery(requeteHQL).list();
        logger.info("Affichage du dernier livre enregistré " + list.get(0).getTitre());
        session.close();
        logger.debug("ça s'est bien passé");
        return list;
        
        }catch(IndexOutOfBoundsException e){
            logger.debug("c'était vide");
            session.close();
            return null;
        }
        
    }
}
