/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.csk.applibibliotheque;

/**
 *
 * @author Celine
 */
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Livre implements Serializable {
    private Integer idLivre;
    private String titre;
    private int annee;
    private String editeur;
    private String nomAuteur;
    private String prenomAuteur;
    private boolean isEmprunte;

    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(Integer idLivre) {
        this.idLivre = idLivre;
    }
    
    @Column(length=100, nullable=false)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Column(nullable = false, precision = 4)
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    @Column(length=100, nullable=false)
    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    @Column(length=50, nullable=false)
    public String getNomAuteur() {
        return nomAuteur;
    }

    public void setNomAuteur(String nomAuteur) {
        this.nomAuteur = nomAuteur;
    }

    @Column(length=30, nullable=false)
    public String getPrenomAuteur() {
        return prenomAuteur;
    }

    public void setPrenomAuteur(String prenomAuteur) {
        this.prenomAuteur = prenomAuteur;
    }

    public boolean isIsEmprunte() {
        return isEmprunte;
    }

    public void setIsEmprunte(boolean isEmprunte) {
        this.isEmprunte = isEmprunte;
    }
    
    
    
}
